package com.sterling.common;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import javax.naming.InitialContext;

import org.apache.log4j.LogManager;
//import javax.jms.ConnectionFactory;

public class ConnectionUtil {
	static org.apache.log4j.Logger logger = LogManager.getLogger(ConnectionUtil.class);
	static Properties properties;
	InitialContext ic = null;

	public static Connection createConnection(String db) {
		properties = new Properties();

		Connection connection = null;
		try {
			properties.load(new FileInputStream("db_credentials.properties"));
		} catch (IOException e) {
			e.getMessage();
		}

		String s = properties.getProperty("db_url");
		String s1 = properties.getProperty("name");
		String s2 = properties.getProperty("pass");
		logger.debug("--url--" + s);
		logger.debug("--name--" + s1);

		try {
			Class.forName("net.sourceforge.jtds.jdbc.Driver");
		} catch (java.lang.ClassNotFoundException e) {
			logger.error("ClassNotFoundException");
			logger.error(e.getMessage());
		}
		try {
			if (CommonConstant.IFN_DB.equalsIgnoreCase(db)) {
				connection = DriverManager.getConnection(properties.getProperty("db_url"),
						properties.getProperty("name"), properties.getProperty("pass"));
				logger.info("Connection created Succesfully with IFN");

			}
		} catch (SQLException e) {
			logger.info(e.getMessage());
		}

		return connection;

	}

	public static Properties getProperties(String filepath) {
		Properties prop = new Properties();
		logger.info("File path :" + filepath);
		try {
			prop.load(new FileInputStream(filepath));
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}

		return prop;
	}

	public static List<String> readFile(String filePath) {
		BufferedReader reader;
		List<String> ids = new ArrayList<>();
		try {
			reader = new BufferedReader(new FileReader(filePath));
			String line = reader.readLine();
			while (line != null) {
				// read next line
				ids.add(line);
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ids;
	}

}
