package com.sterling.service;

import java.io.IOException;

public interface DecompressionService {
 public byte[] getXml() throws IOException;
}
