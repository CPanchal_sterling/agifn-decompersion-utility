package com.sterling.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.sterling.common.CommonConstant;
import com.sterling.common.ConnectionUtil;
import com.sterling.constant.QueryUtil;
import com.sterling.util.DataCompressionUtil;

public class DecompressionServiceImpl implements DecompressionService {
	static Properties properties;
	static Logger logger = LogManager.getLogger(DecompressionServiceImpl.class);

	@Override
	public byte[] getXml() throws IOException {
		Connection connection = null;
		byte[] dataToDeCompress = null;
		byte[] datatoDeCompressOutgoing=null;
		byte[] datatoDeCompressIncoming=null;
		List<String> ifn_request_id = ConnectionUtil.readFile("ifn_request_id.properties");

		connection = ConnectionUtil.createConnection(CommonConstant.IFN_DB);

		logger.info("IFN DB connected");

		try {
			if (ifn_request_id != null && ifn_request_id.size() > 0) {
				String query = QueryUtil.DATA_DECOMPRESS;
				logger.info("Input Query " + query);
				PreparedStatement statement = connection.prepareStatement(query);
				for (int i = 0; i < ifn_request_id.size(); i++) {
					statement.setString(1, ifn_request_id.get(i));
					logger.info("ifn_request id is :" + ifn_request_id.get(i));
					ResultSet rs = statement.executeQuery();
					while (rs.next()) {
						dataToDeCompress = rs.getBytes("Data_request_xml_Comp");
						datatoDeCompressOutgoing=rs.getBytes("Outgoing_Comp");
						datatoDeCompressIncoming=rs.getBytes("Incoming_Comp");
						
						String DecompressedXML = DataCompressionUtil.unCompress2String(dataToDeCompress);
						String DecompressedXMLOutgoing= DataCompressionUtil.unCompress2String(datatoDeCompressOutgoing);
						String DecompressedXMLIncoming=DataCompressionUtil.unCompress2String(datatoDeCompressIncoming);
						try {
							FileInputStream input=new FileInputStream("folder_location.properties");
							Properties prop=new Properties();
							prop.load(input);
							try
							{
							String path=prop.getProperty(("path"))+"decompressedXML"+ifn_request_id.get(i)+".xml";
							File file=new File(path);
							FileOutputStream files = new FileOutputStream(file);
							files.write(DecompressedXML.getBytes());
							files.flush();
							files.close();
							logger.info("Data_Request_Decompressed");
							logger.info("Download completed at this location -->"+prop.getProperty("path"));
							}
							catch(FileNotFoundException e)
							{
								logger.error("error in Data_Request_Decompressed file");
							}
							try
							{
								String path=prop.getProperty(("path"))+"DecompressedXMLOutgoing"+ifn_request_id.get(i)+".xml";
								File file=new File(path);
								FileOutputStream files = new FileOutputStream(file);
								files.write(DecompressedXMLOutgoing.getBytes());
								files.flush();
								files.close();
								logger.info("Connector exchange outgoing");
								logger.info("Download completed at this location -->"+prop.getProperty("path"));
							}
							catch(FileNotFoundException e)
							{
								logger.error("error in Connector exchange outgoing");
							}
							try
							{
								String path=prop.getProperty(("path"))+"DecompressedXMLIncoming"+ifn_request_id.get(i)+".xml";
								File file=new File(path);
								FileOutputStream files = new FileOutputStream(file);
								files.write(DecompressedXMLIncoming.getBytes());
								files.flush();
								files.close();
								logger.info("Connector exchange incoming");
								logger.info("Download completed at this location -->"+prop.getProperty("path"));
							}
							catch(FileNotFoundException e)
							{
								logger.error("error in Connector exchange incoming");
							}
						} catch (FileNotFoundException e) {
							logger.error("location error " + e.getMessage());
						}
					}
					rs.close();
				}
			}

		} catch (SQLException e) {
			logger.error("error in SQL Statement while getting xml " + e.getMessage());

		} finally {
			try {
				connection.close();

			} catch (SQLException e) {
				logger.error("error in getting xml at the time of close connection " + e.getMessage());
			}
		}
		return null;

	}

	public static void main(String[] args) throws IOException {
		DecompressionServiceImpl decompression = new DecompressionServiceImpl();
		decompression.getXml();

	}

}
