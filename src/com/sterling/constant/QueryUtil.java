package com.sterling.constant;

public class QueryUtil {
/* public static String DATA_REQUEST="Select * from IFN_REQUEST_DATA_REQUEST "
		+ "FULL OUTER JOIN DATA_REQUEST "
		+"ON IFN_REQUEST_DATA_REQUEST.Data_request_id=DATA_REQUEST.Data_request_id where Ifn_request_id =?";
*/
	public static String DATA_DECOMPRESS=" SELECT  "
		 + "Data_request_xml_Comp , CONNECTOR_EXCHANGE_OUTGOING.CONTENT_Comp AS 'Outgoing_Comp' , "
		 + "CONNECTOR_EXCHANGE_INCOMING.CONTENT_Comp AS 'Incoming_Comp' "
		 + " FROM IFN_REQUEST_DATA_REQUEST  "
		 + " join DATA_REQUEST ON IFN_REQUEST_DATA_REQUEST.Data_request_id=DATA_REQUEST.Data_request_id "
		 + " join DATA_RESPONSE ON  DATA_REQUEST.Data_request_id=DATA_RESPONSE.Data_request_id "
         + " join CONNECTOR_EXCHANGE_OUTGOING ON "
		 + " DATA_REQUEST.Connector_exchange_outgoing_id = CONNECTOR_EXCHANGE_OUTGOING.Connector_exchange_outgoing_id "
		 + " Join CONNECTOR_EXCHANGE_INCOMING ON "
		 + " CONNECTOR_EXCHANGE_INCOMING.Connector_exchange_incoming_id=DATA_RESPONSE.Connector_exchange_incoming_id "
		 + " where Ifn_request_id=?";
}
