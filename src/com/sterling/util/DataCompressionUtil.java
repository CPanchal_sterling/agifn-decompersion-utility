package com.sterling.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipException;

import org.apache.log4j.Logger;

public class DataCompressionUtil {

	static Logger logger=Logger.getLogger(DataCompressionUtil.class);
	/**
	 * This function compressed the data provided in bytes.
	 * 
	 * @param dataToCompress
	 * @return Returns the data in the compressed form.
	 */
	public static byte[] compress(byte[] dataToCompress) {
		// If the input is null we will not do any processing further
		if(dataToCompress == null )
		{
			return null;
		}
		ByteArrayOutputStream bos = null;
		try {
			bos = new ByteArrayOutputStream();
			GZIPOutputStream gz = new GZIPOutputStream(bos,
					dataToCompress.length);
			gz.write(dataToCompress, 0, dataToCompress.length);
			bos.close();
			gz.close();

		} catch (Exception e) {
			throw new RuntimeException("Could not compress the data", e);
		}
		return bos.toByteArray();
	}

	/**
	 * Overloaded function to accept the String type for Compression.
	 * 
	 * @param dataToCompress
	 * @return
	 */
	public static byte[] compress(String dataToCompress) {
		return compress(dataToCompress.getBytes());
	}

	/**
	 * This method uncompresses the data provided in bytes.
	 * 
	 * @param dataToUncompress
	 * @return The uncompressed byte array is returned.
	 */
	public static byte[] unCompress(byte[] dataToUncompress) {
		// If the input is null we will not do any processing further
		if(dataToUncompress == null )
		{
			return null;
		}

		ByteArrayOutputStream baos = null;
		try {
			InputStream bais = new ByteArrayInputStream(dataToUncompress);
			GZIPInputStream gs = new GZIPInputStream(bais);
			baos = new ByteArrayOutputStream();
			int numBytesRead = 0;
			//byte[] tempBytes = new byte[4096];
			byte[] tempBytes = new byte[bais.available()];
			try {
				while ((numBytesRead = gs.read(tempBytes, 0, tempBytes.length)) != -1) {
					baos.write(tempBytes, 0, numBytesRead);
				}
				bais.close();
				gs.close();
				baos.close();
			} catch (ZipException e) {
				throw new RuntimeException("Could not uncompress the data(1)",
						e);
			}
			
		} catch (Exception e) {
			//throw new RuntimeException("Could not uncompress the data(2)", e);
		} finally {
			if (baos == null)
				throw new RuntimeException("Could not uncompress the data(3).");
		}
		return baos.toByteArray();
	}
	
	/**
	 * This method uncompresses the data and returns String.  
	 * 
	 * @param dataToUncompress
	 * @return
	 */
	public static String unCompress2String(byte[] dataToUncompress) {

		byte[] uncompressedXML = unCompress(dataToUncompress);
		if(uncompressedXML != null){
			return new String(uncompressedXML);
		} else {
			return null;
		}
	}
	
	/* * Read bytes from inputStream and writes to OutputStream, * later converts OutputStream to byte array in Java. */ 
/*	public byte[] toByteArrayUsingJava(S3ObjectInputStream is) throws IOException { 
		ByteArrayOutputStream baos = new ByteArrayOutputStream(); 
		int reads = is.read(); 
		while(reads != -1){ 
			baos.write(reads); 
			reads = is.read(); 
		} 
		return baos.toByteArray(); 
	}
*/
}
